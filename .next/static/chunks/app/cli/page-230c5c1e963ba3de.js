(self.webpackChunk_N_E=self.webpackChunk_N_E||[]).push([[62052],{46413:function(e,n,r){"use strict";/**
 * @license React
 * react-jsx-runtime.production.min.js
 *
 * Copyright (c) Meta Platforms, Inc. and affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */var t=r(52564),o=Symbol.for("react.element"),s=Symbol.for("react.fragment"),i=Object.prototype.hasOwnProperty,u=t.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED.ReactCurrentOwner,c={key:!0,ref:!0,__self:!0,__source:!0};function f(e,n,r){var t,s={},f=null,l=null;for(t in void 0!==r&&(f=""+r),void 0!==n.key&&(f=""+n.key),void 0!==n.ref&&(l=n.ref),n)i.call(n,t)&&!c.hasOwnProperty(t)&&(s[t]=n[t]);if(e&&e.defaultProps)for(t in n=e.defaultProps)void 0===s[t]&&(s[t]=n[t]);return{$$typeof:o,type:e,key:f,ref:l,props:s,_owner:u.current}}n.Fragment=s,n.jsx=f,n.jsxs=f},69219:function(e,n,r){"use strict";e.exports=r(46413)},13520:function(e,n,r){Promise.resolve().then(r.bind(r,52428))},52428:function(e,n,r){"use strict";r.r(n),r.d(n,{default:function(){return s}});var t=r(69219),o=r(57043);function s(){let{data:e}=(0,o.useSession)();return e?(0,t.jsxs)(t.Fragment,{children:["Signed in as ",e.user.email," ",(0,t.jsx)("br",{}),(0,t.jsx)("button",{onClick:()=>(0,o.signOut)(),children:"Sign out"})]}):(0,t.jsxs)(t.Fragment,{children:["Not signed in ",(0,t.jsx)("br",{}),(0,t.jsx)("button",{onClick:()=>(0,o.signIn)(),children:"Sign in"})]})}}},function(e){e.O(0,[57043,9253,66151,1744],function(){return e(e.s=13520)}),_N_E=e.O()}]);