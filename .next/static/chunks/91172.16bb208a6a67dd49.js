(self.webpackChunk_N_E=self.webpackChunk_N_E||[]).push([[91172],{91172:function(){!function(e){function a(a,n){e.languages[a]&&e.languages.insertBefore(a,"comment",{"doc-comment":n})}var n=e.languages.markup.tag,t={pattern:/\/\/\/.*/,greedy:!0,alias:"comment",inside:{tag:n}};a("csharp",t),a("fsharp",t),a("vbnet",{pattern:/'''.*/,greedy:!0,alias:"comment",inside:{tag:n}})}(Prism)}}]);