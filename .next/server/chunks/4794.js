exports.id = 4794;
exports.ids = [4794];
exports.modules = {

/***/ 26809:
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

Promise.resolve(/* import() eager */).then(__webpack_require__.t.bind(__webpack_require__, 3280, 23));
Promise.resolve(/* import() eager */).then(__webpack_require__.t.bind(__webpack_require__, 69274, 23));
Promise.resolve(/* import() eager */).then(__webpack_require__.t.bind(__webpack_require__, 3349, 23));
Promise.resolve(/* import() eager */).then(__webpack_require__.t.bind(__webpack_require__, 90701, 23));
Promise.resolve(/* import() eager */).then(__webpack_require__.t.bind(__webpack_require__, 27977, 23));
Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 19692));
Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 22075));
Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 34451))

/***/ }),

/***/ 28866:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   Z: () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _lib_utils_textConverter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(18157);

const taxonomyFilter = (posts, name, key)=>posts.filter((post)=>//@ts-ignore
        post.frontmatter[name].map((name)=>(0,_lib_utils_textConverter__WEBPACK_IMPORTED_MODULE_0__/* .slugify */ .lV)(name)).includes(key));
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (taxonomyFilter);


/***/ })

};
;