(() => {
var exports = {};
exports.id = 161;
exports.ids = [161];
exports.modules = {

/***/ 18038:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/react");

/***/ }),

/***/ 98704:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/react-dom/server-rendering-stub");

/***/ }),

/***/ 97897:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/react-server-dom-webpack/client");

/***/ }),

/***/ 56786:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/compiled/react/jsx-runtime");

/***/ }),

/***/ 61090:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/app-render/get-segment-param.js");

/***/ }),

/***/ 78652:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/server/future/helpers/interception-routes.js");

/***/ }),

/***/ 53918:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/amp-context.js");

/***/ }),

/***/ 45732:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/amp-mode.js");

/***/ }),

/***/ 3280:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/app-router-context.js");

/***/ }),

/***/ 47244:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/hash.js");

/***/ }),

/***/ 92796:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/head-manager-context.js");

/***/ }),

/***/ 69274:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/hooks-client-context.js");

/***/ }),

/***/ 64486:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/image-blur-svg.js");

/***/ }),

/***/ 50744:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/image-config-context.js");

/***/ }),

/***/ 35843:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/image-config.js");

/***/ }),

/***/ 99552:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/image-loader");

/***/ }),

/***/ 24964:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 11751:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/add-path-prefix.js");

/***/ }),

/***/ 23938:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/format-url.js");

/***/ }),

/***/ 21668:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/handle-smooth-scroll.js");

/***/ }),

/***/ 1897:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-bot.js");

/***/ }),

/***/ 71109:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/is-local-url.js");

/***/ }),

/***/ 28854:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/parse-path.js");

/***/ }),

/***/ 93297:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/remove-trailing-slash.js");

/***/ }),

/***/ 87782:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/router/utils/resolve-href.js");

/***/ }),

/***/ 3349:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/server-inserted-html.js");

/***/ }),

/***/ 82470:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/side-effect.js");

/***/ }),

/***/ 59232:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 40618:
/***/ ((module) => {

"use strict";
module.exports = require("next/dist/shared/lib/utils/warn-once.js");

/***/ }),

/***/ 14916:
/***/ ((module) => {

"use strict";
module.exports = import("next-mdx-remote/rsc");;

/***/ }),

/***/ 54393:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   AppRouter: () => (/* reexport default from dynamic */ next_dist_client_components_app_router__WEBPACK_IMPORTED_MODULE_0___default.a),
/* harmony export */   GlobalError: () => (/* reexport default from dynamic */ next_dist_client_components_error_boundary__WEBPACK_IMPORTED_MODULE_3___default.a),
/* harmony export */   LayoutRouter: () => (/* reexport default from dynamic */ next_dist_client_components_layout_router__WEBPACK_IMPORTED_MODULE_1___default.a),
/* harmony export */   RenderFromTemplateContext: () => (/* reexport default from dynamic */ next_dist_client_components_render_from_template_context__WEBPACK_IMPORTED_MODULE_2___default.a),
/* harmony export */   StaticGenerationSearchParamsBailoutProvider: () => (/* reexport default from dynamic */ next_dist_client_components_static_generation_searchparams_bailout_provider__WEBPACK_IMPORTED_MODULE_8___default.a),
/* harmony export */   __next_app_webpack_require__: () => (/* binding */ __next_app_webpack_require__),
/* harmony export */   actionAsyncStorage: () => (/* reexport safe */ next_dist_client_components_action_async_storage__WEBPACK_IMPORTED_MODULE_6__.actionAsyncStorage),
/* harmony export */   createSearchParamsBailoutProxy: () => (/* reexport safe */ next_dist_client_components_searchparams_bailout_proxy__WEBPACK_IMPORTED_MODULE_9__.createSearchParamsBailoutProxy),
/* harmony export */   decodeAction: () => (/* reexport safe */ react_server_dom_webpack_server_edge__WEBPACK_IMPORTED_MODULE_11__.decodeAction),
/* harmony export */   decodeReply: () => (/* reexport safe */ react_server_dom_webpack_server_edge__WEBPACK_IMPORTED_MODULE_11__.decodeReply),
/* harmony export */   originalPathname: () => (/* binding */ originalPathname),
/* harmony export */   pages: () => (/* binding */ pages),
/* harmony export */   preconnect: () => (/* reexport safe */ next_dist_server_app_render_rsc_preloads__WEBPACK_IMPORTED_MODULE_12__.preconnect),
/* harmony export */   preloadFont: () => (/* reexport safe */ next_dist_server_app_render_rsc_preloads__WEBPACK_IMPORTED_MODULE_12__.preloadFont),
/* harmony export */   preloadStyle: () => (/* reexport safe */ next_dist_server_app_render_rsc_preloads__WEBPACK_IMPORTED_MODULE_12__.preloadStyle),
/* harmony export */   renderToReadableStream: () => (/* reexport safe */ react_server_dom_webpack_server_edge__WEBPACK_IMPORTED_MODULE_11__.renderToReadableStream),
/* harmony export */   requestAsyncStorage: () => (/* reexport safe */ next_dist_client_components_request_async_storage__WEBPACK_IMPORTED_MODULE_5__.requestAsyncStorage),
/* harmony export */   serverHooks: () => (/* reexport module object */ next_dist_client_components_hooks_server_context__WEBPACK_IMPORTED_MODULE_10__),
/* harmony export */   staticGenerationAsyncStorage: () => (/* reexport safe */ next_dist_client_components_static_generation_async_storage__WEBPACK_IMPORTED_MODULE_4__.staticGenerationAsyncStorage),
/* harmony export */   staticGenerationBailout: () => (/* reexport safe */ next_dist_client_components_static_generation_bailout__WEBPACK_IMPORTED_MODULE_7__.staticGenerationBailout),
/* harmony export */   tree: () => (/* binding */ tree)
/* harmony export */ });
/* harmony import */ var next_dist_client_components_app_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(54592);
/* harmony import */ var next_dist_client_components_app_router__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_app_router__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_dist_client_components_layout_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(76301);
/* harmony import */ var next_dist_client_components_layout_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_layout_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_dist_client_components_render_from_template_context__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(57431);
/* harmony import */ var next_dist_client_components_render_from_template_context__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_render_from_template_context__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_dist_client_components_error_boundary__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(52673);
/* harmony import */ var next_dist_client_components_error_boundary__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_error_boundary__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_dist_client_components_static_generation_async_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(30094);
/* harmony import */ var next_dist_client_components_static_generation_async_storage__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_static_generation_async_storage__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_dist_client_components_request_async_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(24437);
/* harmony import */ var next_dist_client_components_request_async_storage__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_request_async_storage__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var next_dist_client_components_action_async_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(46127);
/* harmony import */ var next_dist_client_components_action_async_storage__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_action_async_storage__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var next_dist_client_components_static_generation_bailout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(45486);
/* harmony import */ var next_dist_client_components_static_generation_bailout__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_static_generation_bailout__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var next_dist_client_components_static_generation_searchparams_bailout_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(86404);
/* harmony import */ var next_dist_client_components_static_generation_searchparams_bailout_provider__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_static_generation_searchparams_bailout_provider__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var next_dist_client_components_searchparams_bailout_proxy__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(95486);
/* harmony import */ var next_dist_client_components_searchparams_bailout_proxy__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_searchparams_bailout_proxy__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var next_dist_client_components_hooks_server_context__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(63332);
/* harmony import */ var next_dist_client_components_hooks_server_context__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(next_dist_client_components_hooks_server_context__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var react_server_dom_webpack_server_edge__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(27902);
/* harmony import */ var next_dist_server_app_render_rsc_preloads__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(93099);
/* harmony import */ var next_dist_server_app_render_rsc_preloads__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(next_dist_server_app_render_rsc_preloads__WEBPACK_IMPORTED_MODULE_12__);

    const tree = {
        children: [
        '',
        {
        children: [
        'enroll',
        {
        children: ['__PAGE__', {}, {
          page: [() => Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 8520)), "/Users/jinsup/Desktop/hello-job/src/app/enroll/page.tsx"],
          
        }]
      },
        {
          'loading': [() => Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 71634)), "/Users/jinsup/Desktop/hello-job/src/app/enroll/loading.js"],
          
        }
      ]
      },
        {
          'layout': [() => Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 40054)), "/Users/jinsup/Desktop/hello-job/src/app/layout.tsx"],
'loading': [() => Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 22516)), "/Users/jinsup/Desktop/hello-job/src/app/loading.js"],
'not-found': [() => Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 95654)), "/Users/jinsup/Desktop/hello-job/src/app/not-found.tsx"],
          
        }
      ]
      }.children;
    const pages = ["/Users/jinsup/Desktop/hello-job/src/app/enroll/page.tsx"];

    
    
    
    

    

    
    

    
    
    

    

    
    const __next_app_webpack_require__ = __webpack_require__
    

    const originalPathname = "/enroll/page"
  

/***/ }),

/***/ 48211:
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

Promise.resolve(/* import() eager */).then(__webpack_require__.bind(__webpack_require__, 51590))

/***/ }),

/***/ 51590:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ Register)
});

// EXTERNAL MODULE: external "next/dist/compiled/react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(56786);
// EXTERNAL MODULE: ./node_modules/@uiw/react-md-editor/markdown-editor.css
var markdown_editor = __webpack_require__(32700);
// EXTERNAL MODULE: ./src/app/enroll/markdown.css
var markdown = __webpack_require__(20774);
// EXTERNAL MODULE: ./node_modules/next/dist/shared/lib/app-dynamic.js
var app_dynamic = __webpack_require__(45319);
var app_dynamic_default = /*#__PURE__*/__webpack_require__.n(app_dynamic);
// EXTERNAL MODULE: external "next/dist/compiled/react"
var react_ = __webpack_require__(18038);
// EXTERNAL MODULE: ./node_modules/react-icons/ai/index.js
var ai = __webpack_require__(64647);
// EXTERNAL MODULE: ./node_modules/react-icons/ti/index.js
var ti = __webpack_require__(74429);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(31621);
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);
// EXTERNAL MODULE: ./node_modules/react-icons/ai/index.esm.js
var index_esm = __webpack_require__(19722);
// EXTERNAL MODULE: ./node_modules/next/image.js
var next_image = __webpack_require__(48421);
var image_default = /*#__PURE__*/__webpack_require__.n(next_image);
;// CONCATENATED MODULE: ./src/app/enroll/gift.png
/* harmony default export */ const gift = ({"src":"/_next/static/media/gift.f5b004fc.png","height":512,"width":512,"blurDataURL":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAMAAADz0U65AAAAWlBMVEVMaXGjbCtRJ93OnUmeK2xjLti5Q8iCUJ2rS81NJuCsZC1RJN5IG9KaKIRbNOKbO9KiPDKrYiqJPsq4RaF4PK7VmlHCTpyjPHzNYGxrON6qQjV3Oe7je3S4WEvSqSv+AAAAGXRSTlMAF/Mh/KU+Cv4e6t0uOO5FmNL9/P3o/f2kat0bdgAAAAlwSFlzAAALEwAACxMBAJqcGAAAAEJJREFUeJwVykcCgCAQwMBQZBfsXVD//00lhzkFIIzU3LUuTQS3P+eWhw6T3kOzjUgqXrMVZC5e71bATF7rDPTh9wNbvQJi0H/mxwAAAABJRU5ErkJggg==","blurWidth":8,"blurHeight":8});
// EXTERNAL MODULE: ./node_modules/swr/core/dist/index.mjs + 1 modules
var dist = __webpack_require__(68149);
// EXTERNAL MODULE: ./node_modules/@mantine/core/cjs/index.js
var cjs = __webpack_require__(13162);
// EXTERNAL MODULE: ./node_modules/@uploadthing/react/dist/index.css
var react_dist = __webpack_require__(87555);
// EXTERNAL MODULE: ./node_modules/next-auth/react/index.js
var react = __webpack_require__(63370);
// EXTERNAL MODULE: ./node_modules/@uploadthing/react/dist/index.mjs + 5 modules
var _uploadthing_react_dist = __webpack_require__(58274);
// EXTERNAL MODULE: ./node_modules/next/navigation.js
var navigation = __webpack_require__(59483);
;// CONCATENATED MODULE: ./src/app/enroll/page.tsx
/* __next_internal_client_entry_do_not_use__ default auto */ 

















const MDEditor = app_dynamic_default()(null, {
    loadableGenerated: {
        modules: [
            "../src/app/enroll/page.tsx -> " + "@uiw/react-md-editor"
        ]
    },
    ssr: false
});
// useSWR용 fetcher
// const fetcher = (...args: any[]) => fetch(...args).then((res) => res.json());
const fetcher = async (url)=>{
    const response = await fetch(url);
    const data = await response.json();
    return data;
};
function Register() {
    const [showModal, setShowModal] = (0,react_.useState)(false);
    const [showChooseFile, setShowChooseFile] = (0,react_.useState)(true);
    const [images, setImages] = (0,react_.useState)([]);
    const [form, setForm] = (0,react_.useState)({
        title: "",
        category: "",
        tags: ""
    });
    const [content, setMd] = (0,react_.useState)("# Hello World");
    const { data: session, status } = (0,react.useSession)();
    //태그 데이터 가져오기
    const { data, error, isLoading } = (0,dist/* default */.ZP)("/api/tags", fetcher);
    // 태그 선택용
    const [getTags, setGetTags] = (0,react_.useState)([]);
    // const [getTags, setGetTags] = useState([]);
    // 새로운 테그 몽고db에 저장용
    const [getSavedTags, setGetSavedTags] = (0,react_.useState)([]);
    // 사용자 선택 태그
    // const [selectedTags, setSelectedTags] = useState([]);
    const [selectedTags, setSelectedTags] = (0,react_.useState)([]);
    //저장후 라우팅을 위한 
    const router = (0,navigation.useRouter)();
    const anchorRef = (0,react_.useRef)(null);
    const [divClick, setDivClick] = (0,react_.useState)(false);
    (0,react_.useEffect)(()=>{
        if (data) {
            setGetSavedTags(data.data);
            let parsedTags = data.data.map((tag)=>{
                return {
                    value: tag.name,
                    label: tag.name
                };
            });
            setGetTags(parsedTags);
            console.log("parsedTags=", parsedTags);
        }
    }, [
        data
    ]);
    // useSWR
    if (error) return /*#__PURE__*/ jsx_runtime_.jsx("div", {
        children: "failed to load"
    });
    if (isLoading || !data) return /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
        children: [
            "loading...",
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                type: "button",
                className: "... bg-indigo-500",
                disabled: true,
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("svg", {
                        className: "... mr-3 h-5 w-5 animate-spin",
                        viewBox: "0 0 24 24"
                    }),
                    "Processing..."
                ]
            })
        ]
    });
    let createdBy = "";
    if (status === "authenticated") {
        createdBy = session?.user?.email ?? "";
    }
    const contentType = "application/json";
    const href = "/";
    // 몽고DB에 저장
    const onClick = async (req, res)=>{
        console.log("getTags =", getTags);
        console.log("selectedTags =", selectedTags);
        // 받은 태그와 새로 입력된 태그 비교해서 추가된 태그만 몽고db에 저장
        const differenceArray = getTags.filter((item1)=>!getSavedTags.some((item2)=>item1.value === item2.name));
        const newArray = differenceArray.map((item)=>({
                name: item.value
            }));
        try {
            let response = await fetch("/api/posts", {
                method: "POST",
                headers: {
                    Accept: contentType,
                    "Content-Type": contentType
                },
                body: JSON.stringify({
                    ...form,
                    content,
                    createdBy
                })
            });
            // 태그 데이터 처리
            response = await fetch("/api/tags", {
                method: "POST",
                headers: {
                    Accept: contentType,
                    "Content-Type": contentType
                },
                // body: JSON.stringify({ ...form }),
                body: JSON.stringify(newArray)
            });
            if (response.status >= 400) {
                return res.status(400).json({
                    error: "There was an error"
                });
            }
            return res.status(200).json({
                status: "ok"
            });
        } catch (error) {
            console.log(error);
        }
    };
    const onCancel = ()=>{
        setImages([]);
        console.log(content);
    };
    const handleAnchorClick = async (event)=>{
        event.preventDefault();
        if (!divClick) {
            // Save your data logic here
            // ...
            await onClick(event.target, event);
            // Navigate to a different route
            router.push("/community");
        }
    };
    const handleDivClick = ()=>{
        setDivClick(true);
        if (anchorRef.current) {
            anchorRef.current.click();
        }
    };
    const titleUpload = images.length ? /*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                children: "‣ Upload Complete!"
            }),
            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                className: "mt-2 font-semibold",
                children: [
                    "‣ 업로드된 파일수: ",
                    images.length,
                    " files"
                ]
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                className: "mt-2 font-semibold",
                children: "‣ 이미지 저장경로"
            })
        ]
    }) : null;
    const imgList = /*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
        children: /*#__PURE__*/ jsx_runtime_.jsx("ul", {
            className: "pl-4",
            children: images.map((image)=>/*#__PURE__*/ jsx_runtime_.jsx("li", {
                    className: "mt-2",
                    children: /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                        href: image.fileUrl,
                        target: "_blank",
                        children: image.fileUrl
                    })
                }, image.fileUrl))
        })
    });
    const onModalCloseHandler = ()=>{
        setShowModal(false);
        setShowChooseFile(true);
    };
    // const [imageFile, setImageFile] = useState(null);
    // const handleImageUpload = (event) => {
    //   const file = event.target.files[0];
    //   setImageFile(file);
    // };
    return /*#__PURE__*/ jsx_runtime_.jsx("div", {
        children: /*#__PURE__*/ jsx_runtime_.jsx("section", {
            className: "section-sm",
            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                className: "container",
                children: [
                    /*#__PURE__*/ jsx_runtime_.jsx("div", {
                        className: "row",
                        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: "mx-auto  md:col-10",
                            children: [
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "mb-3",
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                            htmlFor: "title",
                                            className: "form-label",
                                            children: [
                                                "포트폴리오 제목 ",
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "text-red-500",
                                                    children: "*"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("input", {
                                            id: "title",
                                            className: "form-input",
                                            placeholder: "나의 포트폴리오 제목",
                                            type: "text",
                                            value: form.title,
                                            onChange: (event)=>setForm({
                                                    ...form,
                                                    title: event.target.value
                                                })
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "mb-3",
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                            htmlFor: "category",
                                            className: "form-label",
                                            children: [
                                                "카테고리 ",
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "text-red-500",
                                                    children: "*"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("select", {
                                            "data-te-select-init": true,
                                            id: "category",
                                            className: "form-input",
                                            placeholder: "프런트엔드 or 백엔드 ",
                                            value: form.category,
                                            onChange: (event)=>setForm({
                                                    ...form,
                                                    category: event.target.value
                                                }),
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                    value: "1",
                                                    children: "프런트엔드"
                                                }),
                                                /*#__PURE__*/ jsx_runtime_.jsx("option", {
                                                    value: "2",
                                                    children: "백엔드"
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "mb-4",
                                    children: [
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("label", {
                                            htmlFor: "tags",
                                            className: "form-label",
                                            children: [
                                                "태그 ",
                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                    className: "text-red-500",
                                                    children: "*"
                                                })
                                            ]
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx(cjs.MultiSelect, {
                                            mb: "sm",
                                            data: getTags,
                                            value: selectedTags,
                                            onChange: setSelectedTags,
                                            placeholder: "Select or type categories",
                                            searchable: true,
                                            creatable: true,
                                            getCreateLabel: (query)=>`+ Create ${query}`,
                                            onCreate: (query)=>{
                                                const item = {
                                                    value: query,
                                                    label: query
                                                };
                                                setGetTags((current)=>[
                                                        ...current,
                                                        item
                                                    ]);
                                                return item;
                                            },
                                            maxSelectedValues: 3
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "mt-1 border border-slate-600",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(MDEditor, {
                                        style: {
                                            padding: 10
                                        },
                                        height: 500,
                                        value: content
                                    })
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: "mx-10 mt-3 flex flex-auto justify-start",
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                            href: "/",
                                            passHref: true,
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                className: "ml-3 rounded bg-blue-500 px-6 py-3 text-blue-100 no-underline hover:bg-blue-600 hover:text-blue-200 hover:underline",
                                                onClick: handleDivClick,
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                    ref: anchorRef,
                                                    onClick: handleAnchorClick,
                                                    className: "cursor-pointer",
                                                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                        className: "flex flex-nowrap",
                                                        children: [
                                                            /*#__PURE__*/ jsx_runtime_.jsx(ai/* AiOutlineCloudUpload */.IEK, {
                                                                size: "26",
                                                                color: "#fff"
                                                            }),
                                                            /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                                className: "ml-1",
                                                                children: "등록"
                                                            })
                                                        ]
                                                    })
                                                })
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx((link_default()), {
                                            href: href,
                                            onClick: onCancel,
                                            className: "ml-2 rounded bg-blue-500 px-6 py-3 text-blue-100 no-underline hover:bg-blue-600 hover:text-blue-200 hover:underline",
                                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                                className: "flex flex-nowrap",
                                                children: [
                                                    /*#__PURE__*/ jsx_runtime_.jsx(ti/* TiCancelOutline */.QZJ, {
                                                        size: "26",
                                                        color: "#fff"
                                                    }),
                                                    /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                                        className: "ml-1",
                                                        children: "취소"
                                                    })
                                                ]
                                            })
                                        }),
                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("button", {
                                            className: "flex h-12  items-center justify-center gap-2 rounded-md border border-blue-100 px-6 font-bold text-gray-800 outline-none hover:bg-black hover:text-white hover:shadow-lg focus:outline-none active:bg-black",
                                            type: "button",
                                            onClick: ()=>setShowModal(true),
                                            children: [
                                                /*#__PURE__*/ jsx_runtime_.jsx(index_esm/* AiFillFire */.o$9, {
                                                    className: "text-xl"
                                                }),
                                                "Upload Images"
                                            ]
                                        })
                                    ]
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                    className: "pl-10 pt-2",
                                    children: imgList
                                })
                            ]
                        })
                    }),
                    showModal ? // <div className="">
                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                        className: "absolute -bottom-10  ml-40   flex h-auto w-1/2 flex-col items-center justify-center rounded-lg bg-slate-200 p-3 shadow-xl",
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx((image_default()), {
                                src: gift,
                                width: 100,
                                height: 100,
                                alt: "modal"
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                className: "mx-4 mt-2 text-center text-base font-semibold text-gray-400",
                                children: "업로드할 이미지를 선택해주세요."
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: "flex gap-5",
                                children: [
                                    showChooseFile && /*#__PURE__*/ jsx_runtime_.jsx(_uploadthing_react_dist/* UploadButton */.p, {
                                        endpoint: "imageUploader",
                                        onClientUploadComplete: (res)=>{
                                            if (res) {
                                                setImages(res);
                                                const json = JSON.stringify(res);
                                                // Do something with the response
                                                setShowChooseFile(false);
                                                console.log(json);
                                            }
                                        //alert("Upload Completed");
                                        },
                                        onUploadError: (error)=>{
                                            // Do something with the error.
                                            alert(`ERROR! ${error.message}`);
                                        }
                                    }),
                                    !showChooseFile && /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "flex-col",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                children: titleUpload
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                                children: imgList
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx("button", {
                                                className: "focus:shadow-outline mt-2 h-12 rounded-lg bg-indigo-400 px-4 py-2 text-lg font-bold text-indigo-100 transition-colors duration-150 hover:bg-indigo-500",
                                                onClick: onModalCloseHandler,
                                                children: "닫기"
                                            })
                                        ]
                                    })
                                ]
                            })
                        ]
                    }) : null
                ]
            })
        })
    });
}


/***/ }),

/***/ 71634:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Loading)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(56786);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _layouts_components_LoadingSkeleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(27524);


function Loading() {
    // You can add any UI inside Loading, including a Skeleton.
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_layouts_components_LoadingSkeleton__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z, {});
}


/***/ }),

/***/ 8520:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   $$typeof: () => (/* binding */ $$typeof),
/* harmony export */   __esModule: () => (/* binding */ __esModule),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var next_dist_build_webpack_loaders_next_flight_loader_module_proxy__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(21313);

const proxy = (0,next_dist_build_webpack_loaders_next_flight_loader_module_proxy__WEBPACK_IMPORTED_MODULE_0__.createProxy)(String.raw`/Users/jinsup/Desktop/hello-job/src/app/enroll/page.tsx`)

// Accessing the __esModule property and exporting $$typeof are required here.
// The __esModule getter forces the proxy target to create the default export
// and the $$typeof value is for rendering logic to determine if the module
// is a client boundary.
const { __esModule, $$typeof } = proxy;
const __default__ = proxy.default;


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (__default__);

/***/ }),

/***/ 32700:
/***/ (() => {



/***/ }),

/***/ 20774:
/***/ (() => {



/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [683,1519,8149,717,4956,8820,7001,8657], () => (__webpack_exec__(54393)));
module.exports = __webpack_exports__;

})();