"use strict";
(() => {
var exports = {};
exports.id = 4811;
exports.ids = [4811];
exports.modules = {

/***/ 97783:
/***/ ((module) => {

module.exports = require("next/dist/compiled/@edge-runtime/cookies");

/***/ }),

/***/ 28530:
/***/ ((module) => {

module.exports = require("next/dist/compiled/@opentelemetry/api");

/***/ }),

/***/ 54426:
/***/ ((module) => {

module.exports = require("next/dist/compiled/chalk");

/***/ }),

/***/ 40252:
/***/ ((module) => {

module.exports = require("next/dist/compiled/cookie");

/***/ }),

/***/ 23961:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  headerHooks: () => (/* binding */ headerHooks),
  originalPathname: () => (/* binding */ originalPathname),
  requestAsyncStorage: () => (/* binding */ requestAsyncStorage),
  routeModule: () => (/* binding */ routeModule),
  serverHooks: () => (/* binding */ serverHooks),
  staticGenerationAsyncStorage: () => (/* binding */ staticGenerationAsyncStorage),
  staticGenerationBailout: () => (/* binding */ staticGenerationBailout)
});

// NAMESPACE OBJECT: ./src/app/api/uploadthing/route.ts
var route_namespaceObject = {};
__webpack_require__.r(route_namespaceObject);
__webpack_require__.d(route_namespaceObject, {
  GET: () => (GET),
  POST: () => (POST)
});

// EXTERNAL MODULE: ./node_modules/next/dist/server/node-polyfill-headers.js
var node_polyfill_headers = __webpack_require__(35387);
// EXTERNAL MODULE: ./node_modules/next/dist/server/future/route-modules/app-route/module.js
var app_route_module = __webpack_require__(29267);
var module_default = /*#__PURE__*/__webpack_require__.n(app_route_module);
// EXTERNAL MODULE: ./node_modules/uploadthing/dist/next.mjs + 4 modules
var next = __webpack_require__(90803);
;// CONCATENATED MODULE: ./src/app/api/uploadthing/core.ts

const f = (0,next/* createUploadthing */.H)();
const auth = (req)=>({
        id: "fakeId"
    }); // Fake auth function
// FileRouter for your app, can contain multiple FileRoutes
const ourFileRouter = {
    // Define as many FileRoutes as you like, each with a unique routeSlug
    imageUploader: f({
        image: {
            maxFileSize: "4MB",
            maxFileCount: 10
        }
    })// Set permissions and file types for this FileRoute
    .middleware(async (req)=>{
        // This code runs on your server before upload
        const user = await auth(req);
        // If you throw, the user will not be able to upload
        if (!user) throw new Error("Unauthorized");
        // Whatever is returned here is accessible in onUploadComplete as `metadata`
        return {
            userId: user.id
        };
    }).onUploadComplete(async ({ metadata, file })=>{
        // This code RUNS ON YOUR SERVER after upload
        console.log("Upload complete for userId:", metadata.userId);
        console.log("file url", file.url);
    }),
    // Takes a 4 2mb images and/or 1 256mb video
    mediaPost: f({
        image: {
            maxFileSize: "2MB",
            maxFileCount: 4
        },
        video: {
            maxFileSize: "256MB",
            maxFileCount: 1
        }
    }).middleware((req)=>auth(req)).onUploadComplete((data)=>console.log("file", data))
};

;// CONCATENATED MODULE: ./src/app/api/uploadthing/route.ts


// Export routes for Next App Router
const { GET, POST } = (0,next/* createNextRouteHandler */.s)({
    router: ourFileRouter
});

;// CONCATENATED MODULE: ./node_modules/next/dist/build/webpack/loaders/next-app-loader.js?page=%2Fapi%2Fuploadthing%2Froute&name=app%2Fapi%2Fuploadthing%2Froute&pagePath=private-next-app-dir%2Fapi%2Fuploadthing%2Froute.ts&appDir=%2FUsers%2Fjinsup%2FDesktop%2Fhello-job%2Fsrc%2Fapp&appPaths=%2Fapi%2Fuploadthing%2Froute&pageExtensions=tsx&pageExtensions=ts&pageExtensions=jsx&pageExtensions=js&basePath=&assetPrefix=&nextConfigOutput=&preferredRegion=&middlewareConfig=e30%3D!

    

    

    

    const options = {"definition":{"kind":"APP_ROUTE","page":"/api/uploadthing/route","pathname":"/api/uploadthing","filename":"route","bundlePath":"app/api/uploadthing/route"},"resolvedPagePath":"/Users/jinsup/Desktop/hello-job/src/app/api/uploadthing/route.ts","nextConfigOutput":""}
    const routeModule = new (module_default())({
      ...options,
      userland: route_namespaceObject,
    })

    // Pull out the exports that we need to expose from the module. This should
    // be eliminated when we've moved the other routes to the new format. These
    // are used to hook into the route.
    const {
      requestAsyncStorage,
      staticGenerationAsyncStorage,
      serverHooks,
      headerHooks,
      staticGenerationBailout
    } = routeModule

    const originalPathname = "/api/uploadthing/route"

    

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [683,188,3851,5981,803], () => (__webpack_exec__(23961)));
module.exports = __webpack_exports__;

})();