"use strict";
(() => {
var exports = {};
exports.id = 1896;
exports.ids = [1896];
exports.modules = {

/***/ 11185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 49200:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ handler)
});

// EXTERNAL MODULE: ./src/lib/dbConnect.js
var dbConnect = __webpack_require__(28371);
// EXTERNAL MODULE: external "mongoose"
var external_mongoose_ = __webpack_require__(11185);
var external_mongoose_default = /*#__PURE__*/__webpack_require__.n(external_mongoose_);
;// CONCATENATED MODULE: ./models/Forums.js

/* 커뮤니티 게시글  */ const ForumSchema = new (external_mongoose_default()).Schema({
    title: {
        type: String,
        required: [
            true,
            "Please provide a name for this post."
        ],
        maxlength: [
            100,
            "Name cannot be more than 100 characters"
        ]
    },
    category: {
        type: String
    },
    tags: {
        type: Array
    },
    content: {
        type: String
    },
    createdBy: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    totalViews: {
        type: Number,
        default: 0
    }
});
/* harmony default export */ const Forums = ((external_mongoose_default()).models.Forum || external_mongoose_default().model("Forum", ForumSchema));

;// CONCATENATED MODULE: ./pages/api/forums/index.js


async function handler(req, res) {
    const { method } = req;
    await (0,dbConnect/* default */.Z)();
    switch(method){
        case "GET":
            try {
                const forums = await Forums.find({}); /* find all the data in our database */ 
                res.status(200).json({
                    success: true,
                    data: forums
                });
            } catch (error) {
                res.status(400).json({
                    success: false
                });
            }
            break;
        case "POST":
            try {
                const forums = await Forums.create(req.body); /* create a new model in the database */ 
                res.status(201).json({
                    success: true,
                    data: forums
                });
            } catch (error) {
                res.status(400).json({
                    success: false
                });
            }
            break;
        default:
            res.status(400).json({
                success: false
            });
            break;
    }
}


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [8371], () => (__webpack_exec__(49200)));
module.exports = __webpack_exports__;

})();