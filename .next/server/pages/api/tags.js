"use strict";
(() => {
var exports = {};
exports.id = 3329;
exports.ids = [3329];
exports.modules = {

/***/ 11185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 30556:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ handler)
});

// EXTERNAL MODULE: ./src/lib/dbConnect.js
var dbConnect = __webpack_require__(28371);
// EXTERNAL MODULE: external "mongoose"
var external_mongoose_ = __webpack_require__(11185);
var external_mongoose_default = /*#__PURE__*/__webpack_require__.n(external_mongoose_);
;// CONCATENATED MODULE: ./models/Tags.js

/* 태그 저장 */ const TagSchema = new (external_mongoose_default()).Schema({
    name: {
        type: String,
        required: [
            true,
            "Please provide a name for this post."
        ],
        maxlength: [
            100,
            "Name cannot be more than 100 characters"
        ]
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});
/* harmony default export */ const Tags = ((external_mongoose_default()).models.Tag || external_mongoose_default().model("Tag", TagSchema));

;// CONCATENATED MODULE: ./pages/api/tags/index.js


async function handler(req, res) {
    const { method } = req;
    // console.log("req.body", req.body);
    await (0,dbConnect/* default */.Z)();
    switch(method){
        case "GET":
            try {
                let tags = await Tags.find({}); /* find all the data in our database */ 
                const uniqueValuesSet = new Set();
                //REMOVE DUPLICATE NAMES...
                tags = tags.filter((item)=>{
                    const isPresentInSet = uniqueValuesSet.has(item.name);
                    uniqueValuesSet.add(item.name);
                    return !isPresentInSet;
                });
                res.status(200).json({
                    success: true,
                    data: tags
                });
            } catch (error) {
                res.status(400).json({
                    success: false
                });
            }
            break;
        case "POST":
            try {
                const tags = await Tags.create(req.body); /* create a new model in the database */ 
                res.status(201).json({
                    success: true,
                    data: tags
                });
            } catch (error) {
                res.status(400).json({
                    success: false
                });
            }
            break;
        default:
            res.status(400).json({
                success: false
            });
            break;
    }
}


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [8371], () => (__webpack_exec__(30556)));
module.exports = __webpack_exports__;

})();