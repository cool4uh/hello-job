import mongoose from 'mongoose'

/* 사용하지 않음 */

const AuthorSchema = new mongoose.Schema({
  name: {
    /* The name of this pet */

    type: String,
    required: [true, 'Please provide a name for this pet.'],
    maxlength: [60, 'Name cannot be more than 60 characters'],
  },
  age: {
    /* Pet's age, if applicable */

    type: Number,
  },
})

export default mongoose.models.Author || mongoose.model('Author', AuthorSchema)
