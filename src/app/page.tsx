import ImageFallback from "@/components/ImageFallback";
import { getListPage } from "@/lib/contentParser";
import { markdownify } from "@/lib/utils/textConverter";
import CallToAction from "@/partials/CallToAction";
import SeoMeta from "@/partials/SeoMeta";
import Testimonials from "@/partials/Testimonials";
import { FaCheck } from "react-icons/fa/index.js";
import { Feature, Button } from "types";

const Home = () => {
  const homepage = getListPage("_index.md");
  const testimonial = getListPage("sections/testimonial.md");
  const callToAction = getListPage("sections/call-to-action.md");
  const { frontmatter } = homepage;
  const {
    banner,
    features,
  }: {
    banner: { title: string; image: string; content?: string; button?: Button };
    features: Feature[];
  } = frontmatter;

  // console.log(banner.image)

  const bannerTitle = JSON.stringify(markdownify(banner.title).__html).replace(/\"/gi, "") ;
  const bannerContent = JSON.stringify(markdownify(banner.content ?? "").__html).replace(/\"/gi, "");

  return (
    <>
      <SeoMeta />
      {/* <section className="banner-gradient section pt-14"> */}
      <section className="section pt-14">
        <div className="relative container">
          <div className="row justify-center">
            
            {banner.image && (
              <div className="col-12">
                <ImageFallback
                  src={banner.image}
                  width="1272"
                  height="403"
                  alt="banner image"
                  priority
                />
              </div>
            )}
            <div className="absolute px-52 text-center top-1/4 left-1/2 -translate-x-1/2 -translate-y-1/2">
              <div className="text-4xl mb-5 font-bold">{bannerTitle}</div>
              <div className="text-slate-500">{bannerContent}</div>
            </div>
          </div>
        </div>
      </section>

      {features.map((feature, index: number) => (
        <section
          key={index}
          className={`section-sm ${index % 2 === 0 && "bg-gradient"}`}
        >
          <div className="container">
            <div className="row items-center justify-between">
              <div
                className={`mb:md-0 mb-6 md:col-5 ${
                  index % 2 !== 0 && "md:order-2"
                }`}
              >
                <ImageFallback
                  src={feature.image}
                  height={480}
                  width={520}
                  alt={feature.title}
                />
              </div>
              <div
                className={`md:col-7 lg:col-6 ${
                  index % 2 !== 0 && "md:order-1"
                }`}
              >
                <h2
                  className="mb-4"
                  dangerouslySetInnerHTML={markdownify(feature.title)}
                />
                <p
                  className="mb-8 text-lg"
                  dangerouslySetInnerHTML={markdownify(feature.content)}
                />
                <ul>
                  {feature.bulletpoints.map((bullet: string) => (
                    <li className="relative mb-4 pl-6" key={bullet}>
                      <FaCheck className={"absolute left-0 top-1.5"} />
                      <span dangerouslySetInnerHTML={markdownify(bullet)} />
                    </li>
                  ))}
                </ul>
                {feature.button.enable && (
                  <a
                    className="btn btn-primary mt-5"
                    href={feature.button.link}
                  >
                    {feature.button.label}
                  </a>
                )}
              </div>
            </div>
          </div>
        </section>
      ))}

      <Testimonials data={testimonial} />
      <CallToAction data={callToAction} />
    </>
  );
};

export default Home;
