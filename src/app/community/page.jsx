"use client";
import Link from "next/link";
import Slice from "./Slice";
import { useSession, signIn, signOut } from "next-auth/react";
import { useRouter, usePathname, useSearchParams } from 'next/navigation'

const CommunityPage = () => {
  const { data: session } = useSession();
  const router = useRouter();

  const writeHandler = () => {
    if (!session) {
      signIn();
    }
    router.push("/forum-add");
  };

  return (
    <div>
      <section className="section-sm">
        <div className="container">
          {/* 커뮤니티 제목 */}
          <div className="relative my-7 bg-slate-200 bg-gradient-to-r">
            <div className="flex h-full w-full flex-col items-center px-6 py-3 sm:px-8 sm:py-4">
              <div className="w-full text-base font-semibold sm:text-xl">
                커뮤니티
              </div>
            </div>
          </div>

          {/* 커뮤니티 헤딩 */}
          <div className="relative my-6 flex items-center justify-between">
            <div className="hidden flex-none sm:inline">
              {/* <Link
                href="/forum-add"
                onClick={writeHandler}
                className="flex h-9 items-center space-x-1 rounded-md bg-blue-500 px-3 py-2 text-white shadow-sm hover:bg-blue-400 sm:pr-4"
              >
                <span className="inline text-sm font-medium hover:no-underline sm:leading-5">
                  작성하기
                </span>
              </Link> */}
              <button
                onClick={writeHandler}
                className="flex h-9 items-center space-x-1 rounded-md bg-blue-500 px-3 py-2 text-white shadow-sm hover:bg-blue-400 sm:pr-4"
              >
                <span className="inline text-sm font-medium hover:no-underline sm:leading-5">
                  작성하기
                </span>
              </button>
            </div>
            {/* <div className="flex grow flex-col gap-y-4">
              <div className="flex sm:justify-center">
                <div className="scroll-hidden flex space-x-3 overflow-y-scroll lg:space-x-8">
                  사는 얘기
                </div>
                <div className="scroll-hidden flex space-x-3 overflow-y-scroll lg:space-x-8">
                  모임 스터디
                </div>
              </div>
            </div> */}
            <div className="hidden flex-none sm:inline">
              <div className="relative inline-block text-left">
                <div className="">
                  <button className="inline-flex h-9 items-center justify-center space-x-0.5 rounded-md border border-gray-500/30 bg-white px-3.5 py-2 text-gray-700 shadow-sm hover:border-gray-500/70 dark:bg-gray-700 dark:text-gray-300 sm:px-3 sm:pr-4">
                    <span>최신순</span>
                  </button>
                </div>
              </div>
            </div>
          </div>
          {/* 커뮤니티 헤딩 끝*/}
          {/* 게시글 */}
          <Slice />
          <Slice />
          <Slice />
          <Slice />
          <Slice />
          <Slice />
          <Slice />
          {/* 게시글 끝 */}
        </div>
      </section>
    </div>
  );
};

export default CommunityPage;
