import dbConnect from "../../../src/lib/dbConnect";
import Forums from "../../../models/Forums";

export default async function handler(req, res) {
  const { method } = req;

  await dbConnect();

  switch (method) {
    case "GET":
      try {
        const forums = await Forums.find(
          {}
        ); /* find all the data in our database */
        res.status(200).json({ success: true, data: forums });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "POST":
      try {
        const forums = await Forums.create(
          req.body
        ); /* create a new model in the database */
        res.status(201).json({ success: true, data: forums });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
}
