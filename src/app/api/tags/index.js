import dbConnect from "../../../src/lib/dbConnect";
import Tags from "../../../models/Tags";

export default async function handler(req, res) {
  const { method } = req;
  // console.log("req.body", req.body);

  await dbConnect();

  switch (method) {
    case "GET":
      try {
        let tags = await Tags.find({}); /* find all the data in our database */
        const uniqueValuesSet = new Set();

        //REMOVE DUPLICATE NAMES...
        tags = tags.filter((item) => {
          const isPresentInSet = uniqueValuesSet.has(item.name);
          uniqueValuesSet.add(item.name);
          return !isPresentInSet;
        });

        res.status(200).json({ success: true, data: tags });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    case "POST":
      try {
        const tags = await Tags.create(
          req.body
        ); /* create a new model in the database */
        res.status(201).json({ success: true, data: tags });
      } catch (error) {
        res.status(400).json({ success: false });
      }
      break;
    default:
      res.status(400).json({ success: false });
      break;
  }
}
