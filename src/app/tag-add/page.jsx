"use client";
import useSWR from "swr";

import Link from "next/link";
import { useState, useEffect } from "react";
import { AiOutlineCloudUpload } from "react-icons/ai/index";
import { TiCancelOutline } from "react-icons/ti/index";
import { MultiSelect } from "@mantine/core";
import { Swiper, SwiperSlide } from "swiper/react";

const fetcher = (...args) => fetch(...args).then((res) => res.json());

// 태그 선택
export default function TagAdd() {
  //태그 데이터 가져오기
  const { data, error, isLoading } = useSWR("/api/tags", fetcher);
  // 태그 선택용
  const [getTags, setGetTags] = useState([]);
  // 새로운 테그 몽고db에 저장용
  const [getSavedTags, setGetSavedTags] = useState([]);
  // 사용자 선택 태그
  const [selectedTags, setSelectedTags] = useState([]);

  const [form, setForm] = useState({
    name: "",
  });

  useEffect(() => {
    if (data) {
      setGetSavedTags(data.data);
      let parsedTags = data.data.map((tag) => {
        return {
          value: tag.name,
          label: tag.name,
        };
      });
      setGetTags(parsedTags);
    }
  }, [data]);
  // MultiSelect용 배열 만들기

  // useSWR
  if (error) return <div>failed to load</div>;
  if (isLoading) return <div>loading...</div>;

  const contentType = "application/json";
  const href = "/community";

  // 등록 - 몽고DB에 저장
  const onClick = async (req, res) => {
    console.log("getTags =", getTags);
    console.log("selectedTags =", selectedTags);

    // 받은 태그와 새로 입력된 태그 비교해서 추가된 태그만 몽고db에 저장
    const differenceArray = getTags.filter(
      (item1) => !getSavedTags.some((item2) => item1.value === item2.name)
    );

    console.log("differenceArray =", differenceArray);

    // const newArray = getTags.map((item) => ({ name: item.value }));
    const newArray = differenceArray.map((item) => ({ name: item.value }));

    console.log("newArray = ", newArray);
    console.log("form =", form);
    try {
      const response = await fetch("/api/tags", {
        method: "POST",
        headers: {
          Accept: contentType,
          "Content-Type": contentType,
        },
        // body: JSON.stringify({ ...form }),
        body: JSON.stringify(newArray),
      });

      if (response.status >= 400) {
        return res.status(400).json({
          error: "There was an error",
        });
      }

      return res.status(200).json({ status: "ok" });
    } catch (error) {
      console.log(error);
    }
  };
  // 취소
  const onCancel = () => {
    console.log(content);
  };

  return (
    <div>
      <section className="section-sm">
        <div className="container">
          <div className="row">
            <div className="mx-auto md:col-10">
              <div className="mb-4">
                <label htmlFor="tags" className="form-label">
                  태그 <span className="text-red-500">*</span>
                </label>
                <input
                  id="tags"
                  className="form-input"
                  placeholder="태그를 입력해주세요."
                  type="text"
                  value={form.name}
                  onChange={(event) =>
                    setForm({ ...form, name: event.target.value })
                  }
                />
              </div>
              <MultiSelect
                mb="sm"
                label="Tags"
                data={getTags}
                value={selectedTags}
                onChange={setSelectedTags}
                placeholder="Select or type categories"
                searchable
                creatable
                getCreateLabel={(query) => `+ Create ${query}`}
                onCreate={(query) => {
                  const item = { value: query, label: query };
                  setGetTags((current) => [...current, item]);
                  return item;
                }}
                maxSelectedValues={3}
              />
              {/* {data.data.map((item, index) => (
                <div key={index}>{item.name}</div>
              ))} */}
              <div className="mx-10 mt-3 flex justify-start">
                <Link
                  href={href}
                  onClick={onClick}
                  className="ml-3 rounded bg-blue-500 px-6 py-3 text-blue-100 no-underline hover:bg-blue-600 hover:text-blue-200 hover:underline "
                >
                  <div className="flex flex-nowrap">
                    <AiOutlineCloudUpload size="26" color="#fff" />
                    <p className="ml-1">등록</p>
                  </div>
                </Link>
                <Link
                  href={href}
                  onClick={onCancel}
                  className="ml-2 rounded bg-blue-500 px-6 py-3 text-blue-100 no-underline hover:bg-blue-600 hover:text-blue-200 hover:underline"
                >
                  <div className="flex flex-nowrap">
                    <TiCancelOutline size="26" color="#fff" />
                    <p className="ml-1">취소</p>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
