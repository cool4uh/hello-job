<h1 align=center>Nextjs + Tailwind CSS + TypeScript + Tailwind</h1>

<p align=center>당신의 창의력을 발휘할 수 있는 프런트엔드 디자이너와 퍼블리셔 채용의 중심지입니다. 새로운 디자인 세계를 여기에서 탐험하세요!</p>

<p align=center>Made with ♥ by 헬로잡팀</a></p>


### 📦 Dependencies

- next 13.4+
- node v18+
- npm v9.5+
- tailwind v3.3+

### 👉 Development Command

```
npm run dev
```

### 👉 Build Command

```
npm run build
```

